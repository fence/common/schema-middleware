<?php

namespace Glance\SchemaMiddleware\Tests\Unit;

use Glance\SchemaMiddleware\Exception\AppFolderPermissionException;
use Glance\SchemaMiddleware\Exception\InvalidRequestBodyException;
use Glance\SchemaMiddleware\Exception\InvalidSchemaException;
use Glance\SchemaMiddleware\Exception\SchemaFileNotFoundException;
use Glance\SchemaMiddleware\SchemaMiddleware;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Server\RequestHandlerInterface;

final class SchemaMiddlewareTest extends TestCase
{
    public function testFromFileFullPath(): void
    {
        $schemaFile = __DIR__ . "/data/valid-schema.json";
        $factory = new Psr17Factory();

        $middleware = SchemaMiddleware::fromFile($schemaFile, $factory);

        $this->assertEquals(json_decode(file_get_contents($schemaFile)), $middleware->getSchema());
        $this->assertSame($factory, $middleware->getStreamFactory());
    }

    public function testFromFileRelativePath(): void
    {
        $schemaFile = "tests/Unit/data/valid-schema.json";
        $fullPath = __DIR__ . "/data/valid-schema.json";
        $factory = new Psr17Factory();

        $middleware = SchemaMiddleware::fromFile($schemaFile, $factory);

        $this->assertEquals(json_decode(file_get_contents($fullPath)), $middleware->getSchema());
        $this->assertSame($factory, $middleware->getStreamFactory());
    }

    public function testFromFileNotFound(): void
    {
        $schemaFile = __DIR__ . "/data/not-existing-file.json";
        $factory = new Psr17Factory();

        $this->expectException(SchemaFileNotFoundException::class);
        SchemaMiddleware::fromFile($schemaFile, $factory);
    }

    public function testFromFileInvalidJson(): void
    {
        $schemaFile = __DIR__ . "/data/invalid-schema.json";
        $factory = new Psr17Factory();

        $this->expectException(InvalidSchemaException::class);
        SchemaMiddleware::fromFile($schemaFile, $factory);
    }

    public function testValidRequestBody(): void
    {
        $factory = new Psr17Factory();
        $body = $factory->createStream(json_encode([
            "member" => [
                "firstName" => "Mario",
                "lastName"  => "Gunter Simao",
            ]
        ]));
        $request = $factory->createServerRequest("POST", "https://app.cern.ch/api/members");
        $request = $request->withBody($body);
        $fakeResponse = $factory->createResponse();

        $schemaFile = __DIR__ . "/data/valid-schema.json";
        $middleware = SchemaMiddleware::fromFile($schemaFile, $factory);

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn($fakeResponse);

        $response = $middleware->process($request, $handler);
        $this->assertSame($fakeResponse, $response);
    }

    public function testInvalidRequestBody(): void
    {
        $factory = new Psr17Factory();
        $body = $factory->createStream(json_encode([
            "member" => [
                "firstName" => 123,
                "lastName"  => false,
            ]
        ]));
        $request = $factory->createServerRequest("POST", "https://app.cern.ch/api/members");
        $request = $request->withBody($body);
        $fakeResponse = $factory->createResponse();

        $schemaFile = __DIR__ . "/data/valid-schema.json";
        $middleware = SchemaMiddleware::fromFile($schemaFile, $factory);

        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn($fakeResponse);

        $e = new InvalidRequestBodyException([
            [
                "message" => "Integer value found, but a string is required",
                "pointer" => "/member/firstName",
            ],
            [
                "message" => "Boolean value found, but a string is required",
                "pointer" => "/member/lastName",
            ]
        ]);
        $this->expectExceptionObject($e);

        $middleware->process($request, $handler);
    }
}
