<?php

namespace Glance\SchemaMiddleware\Test\Exception;

use Glance\SchemaMiddleware\Exception\SchemaFilePermissionException;
use PHPUnit\Framework\TestCase;

final class SchemaFilePermissionExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new SchemaFilePermissionException("schema.json");

        $this->assertSame("Unable to read schema: schema.json", $e->getMessage());
        $this->assertSame("schema.json", $e->getSchemaPath());
    }
}
