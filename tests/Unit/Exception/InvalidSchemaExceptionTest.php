<?php

namespace Glance\SchemaMiddleware\Test\Exception;

use Glance\SchemaMiddleware\Exception\InvalidSchemaException;
use PHPUnit\Framework\TestCase;

final class InvalidSchemaExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new InvalidSchemaException("schema.json");

        $this->assertSame("Failed to decode schema: schema.json", $e->getMessage());
        $this->assertSame("schema.json", $e->getSchemaPath());
    }
}
