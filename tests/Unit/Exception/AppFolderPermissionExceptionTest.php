<?php

namespace Glance\SchemaMiddleware\Test\Exception;

use Glance\SchemaMiddleware\Exception\AppFolderPermissionException;
use PHPUnit\Framework\TestCase;

final class AppFolderPermissionExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new AppFolderPermissionException();

        $this->assertSame("Unable to read application folder.", $e->getMessage());
    }
}
