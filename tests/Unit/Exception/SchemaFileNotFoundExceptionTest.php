<?php

namespace Glance\SchemaMiddleware\Test\Exception;

use Glance\SchemaMiddleware\Exception\SchemaFileNotFoundException;
use PHPUnit\Framework\TestCase;

final class SchemaFileNotFoundExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new SchemaFileNotFoundException("schema.json");

        $this->assertSame("Unable to find schema: schema.json", $e->getMessage());
        $this->assertSame("schema.json", $e->getSchemaPath());
    }
}
