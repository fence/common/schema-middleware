<?php

namespace Glance\KeycloakMiddleware\Unit\Exception;

use Glance\SchemaMiddleware\Exception\InvalidRequestBodyException;
use PHPUnit\Framework\TestCase;

final class InvalidRequestBodyExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $e = new InvalidRequestBodyException([
            [
                "message" => "Integer value found, but a string is required",
                "pointer" => "/member/firstName",
            ],
            [
                "message" => "Boolean value found, but a string is required",
                "pointer" => "/member/lastName",
            ]
        ]);

        $errors = $e->getErrors();
        $error1 = $errors[0];
        $error2 = $errors[1];

        $this->assertSame(400, $error1->getStatus());
        $this->assertSame("Invalid request body.", $error1->getTitle());
        $this->assertSame("Integer value found, but a string is required", $error1->getDetail());
        $this->assertSame("/member/firstName", $error1->getSource()->getPointer());

        $this->assertSame(400, $error2->getStatus());
        $this->assertSame("Invalid request body.", $error2->getTitle());
        $this->assertSame("Boolean value found, but a string is required", $error2->getDetail());
        $this->assertSame("/member/lastName", $error2->getSource()->getPointer());
    }
}
