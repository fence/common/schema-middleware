<?php

namespace Glance\SchemaMiddleware\Exception;

use Exception;

/**
 * InvalidSchemaException
 *
 * @author Mario Simão <mario.simao@cern.ch>
 */
class InvalidSchemaException extends Exception
{
    /** @var string */
    private $schemaPath;

    public function __construct(string $schemaPath)
    {
        parent::__construct("Failed to decode schema: {$schemaPath}");

        $this->schemaPath = $schemaPath;
    }

    public function getSchemaPath(): string
    {
        return $this->schemaPath;
    }
}
