<?php

namespace Glance\SchemaMiddleware\Exception;

use Exception;

/**
 * AppFolderPermissionException
 *
 * @author Mario Simão <mario.simao@cern.ch>
 */
class AppFolderPermissionException extends Exception
{
    public function __construct()
    {
        parent::__construct("Unable to read application folder.");
    }
}
