<?php

namespace Glance\SchemaMiddleware\Exception;

use Glance\ErrorHandler\BaseException;
use Glance\ErrorHandler\Error;
use Glance\ErrorHandler\ErrorSource;

/**
 * Invalid request body exception
 *
 * Should be used for JSON Schema validation errors
 *
 * @author Mario Simao <mario.simao@cern.ch>
 */
class InvalidRequestBodyException extends BaseException
{
    /**
     * @psalm-param array<array{message: string, pointer: string}> $jsonSchemaErrors
     */
    public function __construct(array $jsonSchemaErrors = [])
    {
        $httpStatus = 400;
        parent::__construct($httpStatus);

        foreach ($jsonSchemaErrors as $jsonSchemaError) {
            $detail = $jsonSchemaError["message"];
            $pointer = $jsonSchemaError["pointer"];

            $source = new ErrorSource($pointer);

            $error = new Error(
                "Invalid request body.",
                $detail,
                null,
                $httpStatus,
                $source
            );

            $this->addError($error);
        }
    }
}
