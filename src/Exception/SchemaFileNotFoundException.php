<?php

namespace Glance\SchemaMiddleware\Exception;

use Exception;

/**
 * SchemaFileNotFoundException
 *
 * @author Mario Simão <mario.simao@cern.ch>
 */
class SchemaFileNotFoundException extends Exception
{
    /** @var string */
    private $schemaPath;

    public function __construct(string $schemaPath)
    {
        parent::__construct("Unable to find schema: {$schemaPath}");

        $this->schemaPath = $schemaPath;
    }

    public function getSchemaPath(): string
    {
        return $this->schemaPath;
    }
}
