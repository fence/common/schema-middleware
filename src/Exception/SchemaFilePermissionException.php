<?php

namespace Glance\SchemaMiddleware\Exception;

use Exception;

/**
 * SchemaFilePermissionException
 *
 * @author Mario Simão <mario.simao@cern.ch>
 */
class SchemaFilePermissionException extends Exception
{
    /** @var string */
    private $schemaPath;

    public function __construct(string $schemaPath)
    {
        parent::__construct("Unable to read schema: {$schemaPath}");

        $this->schemaPath = $schemaPath;
    }

    public function getSchemaPath(): string
    {
        return $this->schemaPath;
    }
}
