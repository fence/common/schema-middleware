<?php

namespace Glance\SchemaMiddleware;

use Glance\SchemaMiddleware\Exception\AppFolderPermissionException;
use Glance\SchemaMiddleware\Exception\InvalidRequestBodyException;
use Glance\SchemaMiddleware\Exception\InvalidSchemaException;
use Glance\SchemaMiddleware\Exception\SchemaFileNotFoundException;
use Glance\SchemaMiddleware\Exception\SchemaFilePermissionException;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use stdClass;

/**
 * Schema Middleware
 *
 * @author Mario Simao <mario.simao@cern.ch>
 */
class SchemaMiddleware implements MiddlewareInterface
{
    /** @var stdClass */
    private $schema;

    /** @var StreamFactoryInterface */
    private $streamFactory;

    private function __construct(stdClass $schema, StreamFactoryInterface $streamFactory)
    {
        $this->schema = $schema;
        $this->streamFactory = $streamFactory;
    }

    public static function fromFile(string $schemaPath, StreamFactoryInterface $streamFactory): self
    {
        if ($schemaPath[0] !== "/") {
            $appBasePath = getcwd();
            if ($appBasePath === false) {
                throw new AppFolderPermissionException();
            }

            $schemaPath = "{$appBasePath}/{$schemaPath}";
        }

        $realPath = realpath($schemaPath);
        if ($realPath === false) {
            throw new SchemaFileNotFoundException($schemaPath);
        }

        $fileContents = file_get_contents($schemaPath);
        if ($fileContents === false) {
            throw new SchemaFilePermissionException($schemaPath);
        }

        /** @var stdClass|null */
        $schema = json_decode($fileContents);
        if ($schema === null) {
            throw new InvalidSchemaException($schemaPath);
        }

        return new self($schema, $streamFactory);
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        /** @var stdClass|null $input */
        $input = json_decode((string) $request->getBody());

        $validator = new Validator();
        $validator->validate(
            $input,
            $this->schema,
            Constraint::CHECK_MODE_APPLY_DEFAULTS | Constraint::CHECK_MODE_COERCE_TYPES
        );

        if (!$validator->isValid()) {
            /** @var array<array{message: string, pointer: string}> $errors */
            $errors = $validator->getErrors();

            throw new InvalidRequestBodyException($errors);
        }

        $parsedBody = $this->streamFactory->createStream(json_encode($input));
        $request = $request->withBody($parsedBody);

        return $handler->handle($request);
    }

    public function getSchema(): stdClass
    {
        return $this->schema;
    }

    public function getStreamFactory(): StreamFactoryInterface
    {
        return $this->streamFactory;
    }
}
